<?php defined('SYSPATH') or die('No direct script access.');

# /Applications/MAMP/bin/php/php5.6.10/bin/php www/index.php --task=Bot:Memory
class Task_Bot_Memory extends Minion_Task
{
    protected $_options = [
    ];
    
    public $key_level = 1;
    public $key_position = 1;

    protected function _execute(array $params)
    {
        Minion_CLI::write("Task start: " . Minion_CLI::color("Bot Memory", 'green'));

        $Models = ORM::factory('User_Point')->find_all();
        foreach ($Models as $item) {
            $this->addMemoryLevelOne($item->id);
        }
    }
 
    private function addMemoryLevelOne($id) {
        $Model = ORM::factory('User_Point', $id);
        if(!$Model->loaded()) {
            Minion_CLI::write("Error: " . Minion_CLI::color("Can't Load Model", 'red'));
        }

        if($Model->isFake == 1) {

            $Data = json_decode($Model->isFakeData, true);
            
            if(!isset($Data['description']))
                return false; 
            
            foreach ($Data['description'] as $key => $item) {

                $values = explode(" ", $item);
                Minion_CLI::write("Load: " . Minion_CLI::color($Model->id, 'green') . " Count: ". Minion_CLI::color(count($values), 'green'));

                foreach ($values as $v_key => $v_item) {

                    $SetModel = ORM::factory('Bot_Memory')->where('name', 'LIKE', $v_item)->find();
                    if($SetModel->loaded()) {
                        
                        $desc = $v_item;
                        $key_level = $SetModel->key_id;
                        if(isset($values[$v_key + 1]))
                            $desc = $desc . " " . $values[$v_key + 1];

                        if(isset($values[$v_key - 1]))
                            $desc = $values[$v_key - 1] . " ". $desc;
                        
                        if($SetModel->description == $desc) {
                            Minion_CLI::write("IGNORE: " . Minion_CLI::color($v_item, 'red') . " description: " . Minion_CLI::color($desc, 'green'));
                            continue;
                        } else {
                            $SetModel = ORM::factory('Bot_Memory');
                            $SetModel->set("key_id", $key_level + 1);
                            $SetModel->set("name", $v_item);
                            $SetModel->set("description", $desc);
                            $SetModel->save();
                            Minion_CLI::write("Level: " . $SetModel->key_id . " | ". Minion_CLI::color($this->key_position. " - ", 'white') . " add: " . Minion_CLI::color($v_item, 'red') . " description: " . Minion_CLI::color($desc, 'green'));
                        }

                    } else {
                        $SetModel = ORM::factory('Bot_Memory');

                        $desc = $v_item;
                        if(isset($values[$v_key + 1]))
                            $desc = $desc . " " . $values[$v_key + 1];

                        if(isset($values[$v_key - 1]))
                            $desc = $values[$v_key - 1] . " ". $desc;

                        $SetModel->set("key_id", $this->key_level);
                        $SetModel->set("name", $v_item);
                        $SetModel->set("description", $desc);
                        $SetModel->save();
                        
                        $this->key_position++;
                        Minion_CLI::write("Level: " . $SetModel->key_id . " | ". Minion_CLI::color($this->key_position. " - ", 'white') . " add: " . Minion_CLI::color($v_item, 'red') . " description: " . Minion_CLI::color($desc, 'green'));
                    }
                    unset($SetModel);
//                    }
                }
                unset($values);
            }
            unset($Data);
        }
    }
}
