<?php defined('SYSPATH') or die('No direct script access.');


Route::set('widgets', 'widgets(/<controller>(/<param>))', array('param' => '.+'))
	->defaults(array(
		'directory' => 'widgets',
		'action'    => 'index',
	));