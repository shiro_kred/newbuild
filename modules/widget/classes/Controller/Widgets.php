<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Widgets extends Controller_Template {

	protected $params = [];


    public function before()
    {
        $this->template = Kohana::$config->load('application')->get('skin') . DIRECTORY_SEPARATOR . $this->template;

        parent::before();

        if(Request::current()->is_initial())
        {
            $this->auto_render = FALSE;
        }

	    if($this->request->param('param') != null)
	    {
		    $this->params = json_decode($this->request->param('param'), true);
	    }
    }


    public function after()
    {

        parent::after();
    }

}  // End Widgets
