<?php defined('SYSPATH') or die('No direct script access.');


class Widget {

    protected $_controllers_folder  = 'widgets';
    protected $_config_filename     = 'widgets';
    protected $_route_name          = 'widgets';
    protected $_params              = array();
    protected $_widget_name;


    public static function factory($widget_name, array $params = NULL, $route_name = NULL)
    {
        return new Widget($widget_name, $params, $route_name);
    }


    public static function load($widget_name, array $params = NULL, $route_name = NULL)
    {
        $widget = new Widget($widget_name, $params, $route_name);
        return $widget->render();
    }


    public function __construct($widget_name, array $params = NULL, $route_name = NULL)
    {

        if ($params != NULL)
        {
            $this->_params = $params;
        }

        if ($route_name != NULL)
        {
            $this->_route_name = $route_name;
        }

        $this->_widget_name = $widget_name;
    }

    public function render()
    {
        $controller = Request::current()->controller();
        $action = Request::current()->action();
        $directory = Request::current()->directory();

        $widget_config = Kohana::$config->load("$this->_config_filename.$this->_widget_name.$controller");

         if ($widget_config != NULL)
         {
             if (in_array($action, $widget_config))
             {
                return NULL;
             }

             if (in_array('all_actions', $widget_config))
             {
                return NULL;
             }
         }

        $this->_params['param'] = json_encode($this->_params, JSON_HEX_TAG);
        $this->_params['controller'] = $this->_widget_name;

		$url = Route::get($this->_route_name)->uri($this->_params);

		return Request::factory($url)->execute();
    }

} 