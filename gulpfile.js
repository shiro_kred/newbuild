var gulp = require('gulp'),
    concat = require('gulp-concat'),
    concatCss = require('gulp-concat-css'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    less = require('gulp-less');

var skin = 'news';

var styles = [
    // Libs
//    'media/' + skin + '/css/bootstrap.min.css',
    
    // Less build
    'www/media/' + skin + '/css/less.css'
];

var scripts = [
    // Libs
    'www/media/' + skin + '/js/libs/jquery/jquery-min.js',
//    'media/' + skin + '/js/bootstrap.min.js'
];


/*** LESS ***/
gulp.task('less', function () {
    gulp.src('www/media/' + skin + '/less/app.less')
        .pipe(less())
        .pipe(concatCss('less.css'))
        .pipe(gulp.dest('www/media/' + skin + '/css/'));
});

/*** CSS ***/
gulp.task('css', ['less'], function () {
    gulp.src(styles)
        .pipe(concatCss('build.css'))
        .pipe(minifyCss())
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest('www/media/' + skin + '/build/'));
});

/*** JS ***/
gulp.task('js', function () {
    gulp.src(scripts)
        .pipe(concat('build.js'))
        .pipe(uglify())
        .pipe(rename('app.min.js'))
        .pipe(gulp.dest('www/media/' + skin + '/build/'));
});



/*** WATCH ***/
gulp.task('watch', function () {
    gulp.watch('www/media/' + skin + '/less/**/*.less', ['css']);
    gulp.watch(styles, ['css']);
    gulp.watch(scripts, ['js']);
});


/*** BUILD ***/
gulp.task('build', ['css', 'js']);