<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'no_review_star'					=> 'Вы не поставили оценку',
    'no_review_text'					=> 'Вы не оставили отзыв',
    'error'					            => 'Произошла ошибка при оставлении отзыва',
    'success'					        => 'Отзыв добавлен',
);
