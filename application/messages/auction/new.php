<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'work_id'				=> 'Тип работ не выбран',
    'auto'					=> 'Авто не выбрано',
    'end'					=> 'Не выбрана дата окончания аукциона',
    'city'					=> 'Город не выбран',
    'category'				=> 'Категория ТС не выбрана',

);