<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'edit'					=> 'Если вы закроите страницу данные не будут сохранены',
    'new'					=> 'Информация о том как и что заполнять',
    'save'					=> 'Запись обновлениа',
    'delete'				=> 'Запись удалена',

);