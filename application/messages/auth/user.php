<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'no_user'					=> 'Неверный email или пароль',
	'no_email'					=> 'Указанный email не найден',
	'username_available'		=> 'Поле ":field" уже существует',
	'email_available'			=> 'Поле ":field" уже существует',
	'unique'					=> 'Такой :field уже существует',
	'email'						=> 'E-mail адрес введен не корректно',
	'success_reg'				=> 'Вы успешно зарегистрированны',
	'success_update'			=> 'Вы успешно обновили свой профиль!',
	'captcha_not_valid'         => 'Не вверно введен защитный код',
	'not_auth'                  => 'Вы не авторизованы',

	// Добавление пользователей
	'user_add_success'          => "Вы зарегистрировались",
	'user_add_fail_password'    => "Пароли не совпадают",
	'user_add_danger'           => "Пользователь не добавлен",
	'user_update_danger'        => "Пользователь не обновлен",
	'user_add_fail'             => "Поле \":field\" введено в неверном формате. Попробуйте указать заного",
	'user_add_empty'            => "Поле \":field\" не может быть пустым",
	'user_send_email'           => "Отправлено приглашение на Email: \":field\"",

	'user_list_no_find'         => "Внимание! Пользователь не существует",
	'user_del'                  => "Готово! Пользователь успешно удален",
	'user_banned'               => "Готово! Пользователь забанен",
	'user_no_banned'            => "Готово! Пользователь разбанен",

	'username' => [
		'unique' => 'Указанный номер привязан к другому аккаунту'
	],

    'Auth/user.username.unique' => 'Данное имя аккаунта используется',
    'Auth/user.email.unique'    => 'Данная электронная почта используется'
);
