<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------
// Load the core Kohana class
require SYSPATH.'classes/Kohana/Core'.EXT;

if (is_file(APPPATH.'classes/Kohana'.EXT))
{
    // Application extends the core
    require APPPATH.'classes/Kohana'.EXT;
}
else
{
    // Load empty core extension
    require SYSPATH.'classes/Kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
date_default_timezone_set('Europe/Moscow');

/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'ru_RU.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Optionally, you can enable a compatibility auto-loader for use with
 * older modules that have not been updated for PSR-0.
 *
 * It is recommended to not enable this unless absolutely necessary.
 */
//spl_autoload_register(array('Kohana', 'auto_load_lowercase'));
/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

/**
 * Set the mb_substitute_character to "none"
 *
 * @link http://www.php.net/manual/function.mb-substitute-character.php
 */
mb_substitute_character('none');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('ru-ru');

if (isset($_SERVER['SERVER_PROTOCOL']))
{
    // Replace the default protocol.
    HTTP::$protocol = $_SERVER['SERVER_PROTOCOL'];
}

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
/*if (isset($_SERVER['KOHANA_ENV']))
{
    Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}*/
if (isset($_SERVER['KOHANA_ENV']))
{
    Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

//Kohana::PRODUCTION – готовый проект;
//Kohana::STAGING – подготовка к релизу;
//Kohana::TESTING – тестирование;
//Kohana::DEVELOPMENT – разработка (по умолчанию).

//Kohana::$environment = Kohana::DEVELOPMENT;
//Kohana::$environment = Kohana::PRODUCTION;
if (dirname(__DIR__) === '/var/www/newbuild') {
    $domain = 'npsh.ru';
    Cookie::$domain = $domain;
    Kohana::$environment = Kohana::PRODUCTION;
    Kohana::$server = Kohana::SERVER_PRODUCT;
} else {
    $domain = 'newbuilder.ru';
    Cookie::$domain = $domain;
    Kohana::$environment = Kohana::DEVELOPMENT;
    Kohana::$server = Kohana::SERVER_LOCAL;
}
/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(array(
    'base_url'    => '/',
    'index_file'  => FALSE,
//    'errors' => TRUE,
    'errors'      => Kohana::$environment !== Kohana::PRODUCTION,
    'profile'     => Kohana::$environment !== Kohana::PRODUCTION,
    'caching'     => Kohana::$environment === Kohana::PRODUCTION,
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH.'logs'));

/**
 * Add Cookie salt
 */
Cookie::$salt = '0LFT012345qwerccc0SHIRO0';


/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules([
    'auth'       => MODPATH.'auth',
//    'cache'      => MODPATH.'cache',
    'database'   => MODPATH.'database',
    'orm'        => MODPATH.'orm',
    'pagination' => MODPATH.'pagination',
    'image'      => MODPATH.'image',
    'message'    => MODPATH.'message',
    'widget'     => MODPATH.'widget',
    'minion'     => MODPATH.'minion',
    'migration'  => MODPATH.'migration',
    'sitemap'    => MODPATH.'sitemap',
//    'bot'        => MODPATH.'bot',

//    'profile'     => MODPATH.'profile',
//    'opening'     => MODPATH.'opening',
//    'game'        => MODPATH.'game',
//    'search'      => MODPATH.'search',
//    'dialogs'           => MODPATH.'dialogs',
    'simplehtmldom'     => MODPATH.'simplehtmldom',
//    'robot'     => MODPATH.'robot'
//    'cron'        => MODPATH.'cron',
]);

/* -------> Eror Route should be first !!! <--------- */
//Route::set('error', 'error/<action>(/<message>)', array('action' => '[0-9]++', 'message' => '.+'))
//     ->defaults(array(
//                    'controller' => 'error',
//                ));

/*** API-V1 запросы ***/
Route::set('v1', 'v1(/<controller>(/<action>(/<id>)))')
    ->defaults(array(
        'directory'  => 'V1',
        'controller' => 'Status',
        'action'     => 'index',
    ));

Route::set('Admin', 'admin(/<controller>(/<action>(/<id>)))')
    ->defaults(array(
        'directory'  => 'Admin',
        'controller' => 'Home',
        'action'     => 'index',
    ));

Route::set('default', '(<controller>(/<action>(/<id>)))')
     ->defaults(array(
                 'directory'  => 'News',
                 'controller' => 'Home',
                 'action'     => 'Index',
                ));
