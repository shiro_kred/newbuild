<?php defined('SYSPATH') OR die('No direct script access.');

class Helper_Social
{


    public static function getSocialList()
    {
        return array(
            'vk' => [
                'name' => 'VK',
                'link' => 'https://vk.com/',
                'icon' => 'fa-vk',
                'value' => 'vk',
                'title' => __('Вконтакте'),
            ],
            'twitch' => [
                'name' => 'Twitch',
                'link' => 'https://www.twitch.tv/',
                'icon' => 'fa-twitch',
                'value' => 'twitch',
                'title' => __('Твичь'),
            ],
            'steam' => [
                'name' => 'Steam',
                'link' => 'http://steamcommunity.com/id/',
                'icon' => 'fa-steam',
                'value' => 'steam',
                'title' => __('Стим'),
            ],
            'youtube' => [
                'name' => 'YouTube',
                'link' => 'https://www.youtube.com/channel/',
                'icon' => 'fa-youtube-play',
                'value' => 'youtube-play',
                'title' => __('Стим'),
            ],
            'twitter' => [
                'name' => 'Twitter',
                'link' => 'https://twitter.com/',
                'icon' => 'fa-twitter',
                'value' => 'twitter',
                'title' => __('Твитер'),
            ],
        );
    }
}