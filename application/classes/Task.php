<?php defined('SYSPATH') or die('No direct script access.');

class Task
{
//    const URL_PHP = '/Applications/MAMP/bin/php/php5.6.10/bin/php';
    const URL_PHP = 'php';

    static function create($task_name, $params = [])
    {
        exec(self::URL_PHP . " index.php --task={$task_name} " . self::params($params) . ' > /dev/null &');
    }

    static function params(array $arr_params)
    {
        $str_params = '';

        if(count($arr_params) > 0)
        {
            foreach($arr_params as $key => $value)
            {
                if(strlen($value) > 0)
                    $str_params .= "--{$key}='{$value}' ";
            }
        }

        return $str_params;
    }
}