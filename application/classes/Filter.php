<?php defined('SYSPATH') OR die('No direct script access.');

class Filter {


    /***
     * Обрезаем строку и делаем ее человекопонятной
     * @param $string
     * @param int $row
     * @return string
     */
    public static function str_row($string, $row = 200) {
        $string = strip_tags($string);
        $string = substr($string, 0, $row);
        $string = rtrim($string, "!,.-");
        $string = substr($string, 0, strrpos($string, ' '));

        return  $string."… ";
    }
}
