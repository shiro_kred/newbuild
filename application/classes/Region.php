<?php defined('SYSPATH') OR die('No direct script access.');

class Region {

    const REGION_TYUMEN         = 'tyumen';
    const REGION_OMSK           = 'omsk';
    const REGION_TOMSK          = 'tomsk';
    const REGION_NOVOSIBIRSK        = 'nsk';

    public static function get_region() {
        return array(
            self::REGION_OMSK,
            self::REGION_TOMSK,
            self::REGION_TYUMEN,
            self::REGION_NOVOSIBIRSK,
        );
    }

    public static function is_region() {
        $ServerName =  explode('.', $_SERVER['SERVER_NAME']);
        $RegionList = Region::get_region();
        $isRegion   = false;
        foreach ($RegionList as $item) {
            if($item == $ServerName[0])
                $isRegion = true;
        }

        return $ServerName[0];
    }
}