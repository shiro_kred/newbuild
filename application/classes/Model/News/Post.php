<?php defined('SYSPATH') or die('No direct script access.');

class Model_News_Post extends ORM {
    public $_table_name = 'news_post';
    
    public function get_image() {
        if($this->image && ($this->image != '')) {
            return $this->image;
        }
        
        return '/media/theme/assets/img/photoback.png';
    }
}

