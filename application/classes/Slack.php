<?php defined('SYSPATH') OR die('No direct script access.');

class Slack {


    const KEY = "xoxp-13762945349-13767828114-94027918165-528c6acab7b5cb1e85b2ddf105fb175e";
    
    public static function sendMsgGeneral($title, $msg) {
        $url = "https://hooks.slack.com/services/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";
        $post = [
            "username"=> "LFT: " . $title,
            "icon_emoji"=> ":main_lft:",
            "text"=> $msg
        ];
        $post = json_encode($post);
        return Curl::query($url, $post);
    }

    public static function sendMsgBeta($title, $msg) {
        $url = "https://hooks.slack.com/services/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";
        $post = [
            "username"=> "LFT: " . $title,
            "icon_emoji"=> ":main_lft:",
            "text"=> $msg
        ];
        $post = json_encode($post);
        return Curl::query($url, $post);
    }
}
