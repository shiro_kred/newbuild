<?php defined('SYSPATH') OR die('No direct script access.');

class Field extends Kohana_Fields {

    
    /***
     * Создаем время конца обенинга
     * @param $val
     * @return int
     */
    public static function createEndTime($val = null) {
        if($val) {
            return time() + $val;
        }
        return time() + parent::TIME_MONTH;
    }

    public static function getNotificationType() {
        return [
            self::NOTIFICATION_TYPE_ALWAYS      => 'Всегда',
            self::NOTIFICATION_TYPE_NEVER       => 'Никогда',
            self::NOTIFICATION_TYPE_ONCE_HOUR   => 'Раз в час',
            self::NOTIFICATION_TYPE_ONCE_DAY    => 'Раз в день',
            self::NOTIFICATION_TYPE_ONCE_WEEK   => 'Раз в неделю',
        ];
    }

    /*** Типы аукционов ***/
    public static function getAuctionType() {
        return [
            self::AUCTION_EMPTY  => '',
            self::AUCTION_AUTOSERVICE  => 'Автосервис',
            self::AUCTION_PART  => 'Автозапчасти',
            self::AUCTION_TECHVIEW  => 'Тех. Осмотр',
        ];
    }

    /*** Типы топлива ***/
    public static function getGasolineType() {
        return [
          self::TYPE_GASOLINE_DIESEL => 'дизель',
          self::TYPE_GASOLINE_PETROL => 'бензин',
          self::TYPE_GASOLINE_PETROLPLUSGAS => 'бензин + газ',
        ];
    }

    /*** Типы коробок ***/
    public static function getEngineType() {
        return [
            self::TYPE_ENGINE_HAND    => 'ручная',
            self::TYPE_ENGINE_MACHINE => 'автоматическая',
        ];
    }

    /*** Типы привода ***/
    public static function getDriveType() {
        return [
            self::TYPE_DRIVE_FRONT    => 'передний привод',
            self::TYPE_DRIVE_REAR     => 'задний привод',
            self::TYPE_DRIVE_FULL     => 'полный привод',
        ];
    }

    /*** Типы точек ***/
    public static function getPointsType() {
        return [
            self::POINT_TYPE_STO_STATION        => 'СТО',
            self::POINT_TYPE_GAS_STATION        => 'АЗС',
            self::POINT_TYPE_MOUNTING_STATION   => 'Шиномонтаж',
            self::POINT_TYPE_CAR_WASH           => 'Автомойки',
            self::POINT_TYPE_CAR_RENTAL         => 'Прокат авто',
            self::POINT_TYPE_AUTO_INSURANCE     => 'Автострахование',
            self::POINT_TYPE_AUTO_SALONS        => 'Автосалоны'
        ];
    }

    /*** Типы работ ***/
    public static function getWorkType() {
        return [
            self::TYPE_WORK_FITTER    => 'Слесарные работы',
            self::TYPE_WORK_BODY     => 'Кузовные работы'
        ];
    }

    /*** Производитель ***/
    public static function getManufacturerType() {
        return [
            self::ZERO    => 'Оригинал',
            self::ONE     => 'Аналог',
            self::TWO     => 'Любой'
        ];
    }

//    /*** Классификация транспортного средства ***/
//    public static function getCategoryAutoType() {
//        return [
//            self::AUCTION_EMPTY       => '',
//            self::CATEGORY_AUTO_M1    => 'M1 - ТС имеющие не более восьми мест для сидения',
//            self::CATEGORY_AUTO_M2    => 'M2 - ТС имеющие более восьми мест для сидения, максимальная масса которых не превышает 5 тонн',
//            self::CATEGORY_AUTO_M3    => 'M3 - ТС имеющие более восьми мест для сидения, максимальная масса которых превышает 5 тонн',
//            self::CATEGORY_AUTO_N1    => 'N1 - ТС массой не более 3,5 тонн',
//            self::CATEGORY_AUTO_N2    => 'N2 - ТС массой свыше 3,5 тонн, но не более 12 тонн',
//            self::CATEGORY_AUTO_N3    => 'N3 - ТС массой более 12 тонн',
//        ];
//    }


//    /*** Типы работ ***/
//    public static function getWorkType() {
//        return [
//            self::TYPE_WORK_FITTER    => 'Слесарные работы',
//            self::TYPE_WORK_BODY     => 'Кузовные работы'
//        ];
//    }

}
