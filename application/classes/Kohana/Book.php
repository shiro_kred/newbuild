<?php defined('SYSPATH') OR die('No direct script access.');

class Kohana_Book {

    const FIELD_HAIR  = 'hair';
    const FIELD_FACE  = 'face';
    const FIELD_EYES  = 'eyes';
    const FIELD_NOSE  = 'nose';
    const FIELD_MOUTH = 'mouth';

    const FILED_SHOULDER = 'shoulder';  //плечи
    const FILED_TORSO    = 'torso';     //торс
    const FILED_WAIST    = 'waist';     //талия
    const FILED_LEGS     = 'legs';      //ноги
    const FILED_HAND     = 'hand';      //ноги

    const FIELD_HEIR_EQUIPMENT      = 'hair_equipment';
    const FILED_SHOULDER_EQUIPMENT  = 'shoulder_equipment';
    const FILED_TORSO_EQUIPMENT     = 'torso_equipment';
    const FILED_WAIST_EQUIPMENT     = 'waist_equipment';
    const FILED_LEGS_EQUIPMENT      = 'legs_equipment';
    const FILED_HAND_EQUIPMENT      = 'hand_equipment';

    const FILED_OTHER_EQUIPMENT     = 'other_equipment';
    const FILED_WEAPON_EQUIPMENT    = 'weapon_equipment';
    
    public static function getAllType() {
        return [
            self::FIELD_HAIR,
            self::FIELD_FACE,
            self::FIELD_EYES,
            self::FIELD_NOSE,
            self::FIELD_MOUTH,
            
            self::FILED_SHOULDER,
            self::FILED_TORSO,
            self::FILED_WAIST,
            self::FILED_LEGS,
            self::FILED_HAND,
            
            self::FIELD_HEIR_EQUIPMENT,
            self::FILED_SHOULDER_EQUIPMENT,
            self::FILED_TORSO_EQUIPMENT,
            self::FILED_WAIST_EQUIPMENT,
            self::FILED_LEGS_EQUIPMENT,
            self::FILED_HAND_EQUIPMENT,
            
            self::FILED_OTHER_EQUIPMENT,
            self::FILED_WEAPON_EQUIPMENT,
        ];
    }

    public static function getPersonalType() {
        return [
            'hair'          => 'волосы',
            'face_shape'    => 'форма лица',
            'eyes'          => 'глаза',
            'nose'          => 'нос',
            'mouth'         => 'рот'
        ];
    }
}