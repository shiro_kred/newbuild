<?php defined('SYSPATH') OR die('No direct script access.');

class Kohana_Fields {

    const AUCTION_EMPTY  = ''; // Работы
    const AUCTION_WORK  = 'work'; // Работы
    const AUCTION_AUTOSERVICE  = 'autoservice'; // Автосервис
    const AUCTION_TECHVIEW  = 'techview'; // ТехОсмотр
    const AUCTION_PART  = 'parts'; // Запчасти
    const AUCTION_BELAY = 'belay'; // Страховка
    const AUCTION_AUTO  = 'auto'; // Авто
    
    const STATUS_CREATE = 'create';
    const STATUS_EDIT = 'edit';
    const STATUS_DELETE = 'delete';

    const TYPE_USER_ACCOUNT = 'account';
    const TYPE_USER_COMPANY = 'company';

    const TYPE_GASOLINE_PETROL = 'petrol';  // Топливо Биензин
    const TYPE_GASOLINE_DIESEL = 'diesel';  // Топливо Дизель
    const TYPE_GASOLINE_PETROLPLUSGAS    = 'pertrolplusgas';  // Топливо Бензин + Газ

    const TYPE_ENGINE_HAND    = 'hand';     // ручная каробка
    const TYPE_ENGINE_MACHINE = 'machine';  // автоматическая каробка

    const TYPE_DRIVE_FRONT = 'front';       // Передний привод
    const TYPE_DRIVE_REAR  = 'rear';        // Задний привод
    const TYPE_DRIVE_FULL  = 'full';        // Полный привод

    const TYPE_WORK_FITTER  = 1;        // Слесарные работы
    const TYPE_WORK_BODY    = 2;        // Полный привод

    const CATEGORY_AUTO_M1  = 1;        // Классификация транспортного средства M1
    const CATEGORY_AUTO_M2  = 2;        // Классификация транспортного средства M2
    const CATEGORY_AUTO_M3  = 3;        // Классификация транспортного средства M3
    const CATEGORY_AUTO_N1  = 4;        // Классификация транспортного средства N1
    const CATEGORY_AUTO_N2  = 5;        // Классификация транспортного средства N2
    const CATEGORY_AUTO_N3  = 6;        // Классификация транспортного средства N3

    const POINT_TYPE_STO_STATION        = 'sto_station';        // СТО
    const POINT_TYPE_GAS_STATION        = 'gas_station';        // АЗС
    const POINT_TYPE_MOUNTING_STATION   = 'mounting_station';   // Шиномантаж
    const POINT_TYPE_CAR_WASH           = 'car_wash';           // Автомойки
    const POINT_TYPE_CAR_RENTAL         = 'car_rental';         // Прокат авто
    const POINT_TYPE_AUTO_INSURANCE     = 'auto_insurance';     // Авто страхование
    const POINT_TYPE_AUTO_SALONS        = 'auto_salons';        // Авто салоны

    const POST_TYPE_LIST    = 'list';
    const POST_TYPE_EDIT    = 'edit';
    const POST_TYPE_DELETE  = 'delete';

    const ZERO  = 0;
    const ONE  = 1;
    const TWO  = 2;


    const NOTIFICATION_TYPE_ALWAYS      = 'always';
    const NOTIFICATION_TYPE_NEVER       = 'never';
    const NOTIFICATION_TYPE_ONCE_HOUR   = 'every_hour';
    const NOTIFICATION_TYPE_ONCE_DAY    = 'once_a_day';
    const NOTIFICATION_TYPE_ONCE_WEEK   = 'once_a_week';
}