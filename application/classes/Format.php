<?php defined('SYSPATH') or die('No direct script access.');

class Format
{
    static function digit($number, $d = ' ')
    {
        return number_format($number, 0, ',', $d);
    }

    static function only_number($number)
    {
        return preg_replace('/[^0-9]/', '', $number);
    }

    static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}