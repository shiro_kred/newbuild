<?php defined('SYSPATH') or die('No direct script access.');

# /Applications/MAMP/bin/php/php5.6.10/bin/php www/index.php --task=Parse:Alltmn
class Task_Parse_Alltmn extends Minion_Task
{
    protected $_options = [
    ];

    public function strFormat($string)
    {
        return preg_replace('|[\s]+|s', ' ', rtrim(trim(str_replace(array("/  +/", "<br>", "<br/>", "\n", "\r"), ' ', $string))));
    }

    protected function _execute(array $params)
    {

        $url = 'http://alltmn.ru';
        require_once( MODPATH.'simplehtmldom/libs/simple_html_dom.php');
        Minion_CLI::write("Start");
        $Post = Curl::query($url . '/novosti/');
        if ($Post) {
            Minion_CLI::write("Post - success ...");
            $html = new simple_html_dom();
            $html->load($Post);

            foreach ($html->find('.news-item') as $key => $item) {
                $item->plaintext = $this->strFormat($item->plaintext);

                $link = $description = preg_replace('/ {2,}/',' ', $item);
                $link = explode('<a href=', $link);
                $link = explode('">', $link[1]);
                $link = str_replace('"', '', $link[0]);
                Minion_CLI::write("$key: Post loader - " . $link);

                $Post = Curl::query($url . $link);
                if($Post) {
                    $html = new simple_html_dom();
                    $html->load($Post);

                    $ModelNews = ORM::factory('News_Post')->where('parse_link', 'LIKE', $url . $link)->find();
                    $ModelNews->set('parse_id', $link);
                    $ModelNews->set('parse_link', $url . $link);
                    
                    $new_lead = '';
                    foreach ($html->find('h1') as $key_photo => $item_photo) {
                        $item_photo = strip_tags($item_photo);
                        $new_lead   = preg_replace('/ {2,}/',' ', $item_photo);
                    }
                    Minion_CLI::write($new_lead);

                    $new_photo = '';
                    $new_description = '';
                    foreach ($html->find('.content') as $key_photo => $item_photo) {
                        $img = preg_replace('/ {2,}/',' ', $item_photo);
                        $img = explode('src=', $img);
                        if(isset($img[1])) {
                            $img = explode('title=', $img[1]);
                            $img = str_replace(['"', ' '], '', $img[0]);
                            $new_photo = $url . $img;    
                        }

                        //$item_photo      = strip_tags($item_photo);
                        //$new_description = preg_replace('/ {2,}/',' ', $item_photo);
                        $new_description = str_replace(['content'], 'context', $item_photo);
                        $new_description = str_replace(['src="'], 'src="'.$url , $item_photo);
                    }

                    $ModelNews->set('title', $new_lead);
                    $ModelNews->set('image', $new_photo);
                    $ModelNews->set('region', Region::REGION_TYUMEN);
                    $ModelNews->set('description', $new_description);
                    Minion_CLI::write("IMAGE: " . $new_photo);
                    Minion_CLI::write("TITLE: " . $new_lead);

                    $ModelNews->save();
                }
            }
            $html->clear();
        }
    }
}