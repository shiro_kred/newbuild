<?php defined('SYSPATH') or die('No direct script access.');

# /Applications/MAMP/bin/php/php5.6.10/bin/php www/index.php --task=Parse:Gorod55
class Task_Parse_Omskinform extends Minion_Task
{
    protected $_options = [
    ];

    public function strFormat($string)
    {
        return preg_replace('|[\s]+|s', ' ', rtrim(trim(str_replace(array("/  +/", "<br>", "<br/>", "\n", "\r"), ' ', $string))));
    }

    protected function _execute(array $params)
    {

        $url = 'http://www.omskinform.ru';
        require_once( MODPATH.'simplehtmldom/libs/simple_html_dom.php');
        Minion_CLI::write("Start");
        $Post = Curl::query($url);
        if ($Post) {
            $html = new simple_html_dom();
            $html->load($Post);

            foreach ($html->find('.n_news') as $key => $item) {

                $item->plaintext = $this->strFormat($item->plaintext);

                $link = explode('<a href=', $item);
                $link = explode(' class="n_img"', $link[1]);
                $link = str_replace('"', '', $link[0]);
                Minion_CLI::write("Load ITEM: " . $link);

                $description = strip_tags($item);
                $description = preg_replace('/ {2,}/',' ', $description);
                Minion_CLI::write("ITEM DESC: " . $description);

                Minion_CLI::write("$key: Post loader...");

                $Post = Curl::query($link);
                if($Post) {
                    $html = new simple_html_dom();
                    $html->load($Post);

                    $ModelNews = ORM::factory('News_Post')->where('parse_link', 'LIKE', $url . $link)->find();
                    $ModelNews->set('parse_id', $link);
                    $ModelNews->set('parse_link', $url . $link);

                    $new_photo = '';
                    foreach ($html->find('.nt_img_handler') as $key_photo => $item_photo) {
                        $item_photo = explode('src=', $item_photo);
                        $item_photo = explode('"', $item_photo[1]);
                        $new_photo  = $item_photo[1];
                        //$new_photo = $item_photo->getAttribute('src');
                    }
                    $ModelNews->set('image', $new_photo);

                    $new_lead = '';
                    foreach ($html->find('.n_cap_lnk_one') as $key_photo => $item_photo) {
                        $item_photo = strip_tags($item_photo);
                        $new_lead   = preg_replace('/ {2,}/',' ', $item_photo);
                    }
                    $ModelNews->set('title', $new_lead);


                    $new_description = '';
                    foreach ($html->find('.n_text') as $key_photo => $item_photo) {
                        //$item_photo      = strip_tags($item_photo);
                        //$new_description = preg_replace('/ {2,}/',' ', $item_photo);
                        $new_description = str_replace([' Фото: hawk.ru', ' Источник: ИА «Город55»&nbsp;'], '', $item_photo);
                    }

                    $ModelNews->set('region', Region::REGION_OMSK);
                    $ModelNews->set('description', $new_description);
                    Minion_CLI::write("IMAGE: " . $new_photo);
                    Minion_CLI::write("TITLE: " . $new_lead);

                    $ModelNews->save();
                }
            }

            $html->clear();
        }
    }
}