<?php defined('SYSPATH') or die('No direct script access.');

# /Applications/MAMP/bin/php/php5.6.10/bin/php www/index.php --task=Parse:Tomske
class Task_Parse_Tomske extends Minion_Task
{
    protected $_options = [
    ];

    public function strFormat($string)
    {
        return preg_replace('|[\s]+|s', ' ', rtrim(trim(str_replace(array("/  +/", "<br>", "<br/>", "\n", "\r"), ' ', $string))));
    }

    protected function _execute(array $params)
    {

        $url = 'http://www.tomsk.ru';
        require_once( MODPATH.'simplehtmldom/libs/simple_html_dom.php');
        Minion_CLI::write("Start");
        $Post = Curl::query($url . '/news');
        if ($Post) {
            Minion_CLI::write("Post - success ...");
            $html = new simple_html_dom();
            $html->load($Post);

            foreach ($html->find('.center-small') as $key => $item) {
                $item->plaintext = $this->strFormat($item->plaintext);

                $link = preg_replace('/ {2,}/',' ', $item);
                $link = explode('<a class="image" href=', $link);
                $link = explode('">', $link[1]);
                $link = str_replace('"', '', $link[0]);
                Minion_CLI::write("Load ITEM: " . $link);
                $Post = Curl::query($url . $link);
                if($Post) {
                    $html = new simple_html_dom();
                    $html->load($Post);

                    $ModelNews = ORM::factory('News_Post')->where('parse_link', 'LIKE', $url . $link)->find();
                    $ModelNews->set('parse_id', $link);
                    $ModelNews->set('parse_link', $url . $link);

                    $new_photo = '';
                    foreach ($html->find('.pic_preview') as $key_photo => $item_photo) {
                        $img = preg_replace('/ {2,}/',' ', $item_photo);
                        $img = explode('src=', $img);
                        $img = explode('alt=', $img[1]);
                        $img = str_replace(['"', ' '], '', $img[0]);
                        $new_photo = $url . $img;
                    }
                    $ModelNews->set('image', $new_photo);

                    $new_lead = '';
                    foreach ($html->find('h1') as $key_photo => $item_photo) {
                        $item_photo = strip_tags($item_photo);
                        $new_lead   = preg_replace('/ {2,}/',' ', $item_photo);
                    }
                    $ModelNews->set('title', $new_lead);

                    $new_description = '';
                    foreach ($html->find('.article-body') as $key_photo => $item_photo) {
                        //$item_photo      = strip_tags($item_photo);
                        //$new_description = preg_replace('/ {2,}/',' ', $item_photo);
                        $new_description = str_replace([' Фото: hawk.ru', ' Источник: ИА «Город55»&nbsp;'], '', $item_photo);
                    }

                    $ModelNews->set('region', Region::REGION_TOMSK);
                    $ModelNews->set('description', $new_description);
                    Minion_CLI::write("IMAGE: " . $new_photo);
                    Minion_CLI::write("TITLE: " . $new_lead);
//                    Minion_CLI::write("DESC: " . $new_description);
                    
                    $ModelNews->save();
                }
            }
            $html->clear();
        }
    }
}