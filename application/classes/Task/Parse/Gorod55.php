<?php defined('SYSPATH') or die('No direct script access.');

# /Applications/MAMP/bin/php/php5.6.10/bin/php www/index.php --task=Parse:Gorod55
class Task_Parse_Gorod55 extends Minion_Task
{
    protected $_options = [
    ];

    public function strFormat($string)
    {
        return preg_replace('|[\s]+|s', ' ', rtrim(trim(str_replace(array("/  +/", "<br>", "<br/>", "\n", "\r"), ' ', $string))));
    }
    
    protected function _execute(array $params)
    {

        $url = 'http://gorod55.ru';
        require_once( MODPATH.'simplehtmldom/libs/simple_html_dom.php');
        Minion_CLI::write("Start");
        $Post = Curl::query($url . '/news/gorod');
        if ($Post) {
            $html = new simple_html_dom();
            $html->load($Post);

            foreach ($html->find('.news_item') as $key => $item) {

                $item->plaintext = $this->strFormat($item->plaintext);
                
                $link = explode('<a href=', $item);
                $link = explode(' target="_blank"', $link[1]);
                $link = str_replace('"', '', $link[0]);
                Minion_CLI::write("Load ITEM: " . $link);

                $description = strip_tags($item);
                $description = preg_replace('/ {2,}/',' ', $description);
                Minion_CLI::write("ITEM DESC: " . $description);

                Minion_CLI::write("$key: Post loader...");

                $Post = Curl::query($url . $link);
                if($Post) {
                    $html = new simple_html_dom();
                    $html->load($Post);

                    $ModelNews = ORM::factory('News_Post')->where('parse_link', 'LIKE', $url . $link)->find();
                    $ModelNews->set('parse_id', $link);
                    $ModelNews->set('parse_link', $url . $link);

                    $new_photo = '';
                    foreach ($html->find('.news-photo') as $key_photo => $item_photo) {
                        $item_photo->plaintext = $this->strFormat($item_photo->plaintext);
                        $new_photo = $item_photo->getAttribute('src');
                    }
                    $ModelNews->set('image', $new_photo);

                    $new_lead = '';
                    foreach ($html->find('.item_lead') as $key_photo => $item_photo) {
                        $item_photo = strip_tags($item_photo);
                        $new_lead   = preg_replace('/ {2,}/',' ', $item_photo);
                    }
                    $ModelNews->set('title', $new_lead);


                    $new_description = '';
                    foreach ($html->find('.news-description') as $key_photo => $item_photo) {
                        //$item_photo      = strip_tags($item_photo);
                        //$new_description = preg_replace('/ {2,}/',' ', $item_photo);
                        $new_description = str_replace([' Фото: hawk.ru', ' Источник: ИА «Город55»&nbsp;'], '', $item_photo);
                    }
                    $ModelNews->set('region', Region::REGION_OMSK);
                    $ModelNews->set('description', $new_description);
                    Minion_CLI::write("IMAGE: " . $new_photo);
                    Minion_CLI::write("TITLE: " . $new_lead);
                    $ModelNews->save();
                }
            }

            $html->clear();
        }
    }
}