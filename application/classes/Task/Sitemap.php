<?php defined('SYSPATH') or die('No direct script access.');

# /Applications/MAMP/bin/php/php5.6.10/bin/php www/index.php --task=Sitemap
class Task_Sitemap extends Minion_Task
{

    const SITEMAP_TITLE_CONTENT_GENERATE = 'Start create sitemap for : ';
    const SITEMAP_TITLE_CONTENT_END = 'success';
    const SITEMAP_URL_CONTENT = 'http://avtobook.info';
    const SITEMAP_PRIORITY_10 = '1';
    const SITEMAP_PRIORITY_9  = '0.9';
    const SITEMAP_PRIORITY_8  = '0.8';
    const SITEMAP_LAST_MOD_ALWAYS = 'always';
    public $template = "frontend/v_sitemap";


    protected $_options = [
    ];

    protected function _execute(array $params)
    {
        Minion_CLI::write("Start task: " . Minion_CLI::color("Generate SiteMap", 'green'));

        $server = Kohana::$config->load('application')->get('server');
        foreach (Region::get_region() as $item_region) {
            Minion_CLI::write("Start:" . Minion_CLI::color($item_region, 'green'));
            
            $data = ORM::factory('News_Post')
                ->where('region', 'LIKE', $item_region)
                ->order_by('date', 'DESC')
                ->find_all();

            $sitemap = new Sitemap;
            $url = new Sitemap_URL;
            
            foreach ($data as $key => $item) {
                Minion_CLI::write($item->id . " - " . Minion_CLI::color($item->region, 'green'));

                $newUrl = 'http://' . $item->region . '.' . $server. '/blog/item/' . $item->id;
                $url->set_loc($newUrl)
                    ->set_priority(self::SITEMAP_PRIORITY_9)
                    ->set_last_mod(time())
                    ->set_change_frequency(self::SITEMAP_LAST_MOD_ALWAYS);
                $sitemap->add($url);

            }

            $response = urldecode($sitemap->render());
            $this->FilePutContents($item_region, $response);
        }
        
        
        
    }

    /**
     * Создаем sitemap по массиву данных
     * @param $type
     * @throws Kohana_Exception
     * @return boolean
     */
    public function createResponse($type) {
        
        $count = ORM::factory('User_Point')->where('type_point', '=', $type)->count_all();
        Minion_CLI::write($type." find " . Minion_CLI::color("count: ". $count, 'green'));
        
        if($count == 0) {
            return false;
        }

        $model = ORM::factory('User_Point')->where('type_point', '=', $type)->find_all();
        
        $response = $this->getList($type, $model);

        if($this->FilePutContents($type, $response)) {
            Minion_CLI::write("SUCCESS: " . Minion_CLI::color("Generate ". $type, 'green'));
        } else {
            Minion_CLI::write("FAIL: " . Minion_CLI::color("Generate ". $type, 'red'));
        }

        unset($model);
        unset($response);
        
        return true;
    }

    /**
     * Способ генерации карты сайта
     * @param $name
     * @param $model
     * @return string
     */
    public function getList($name, $model) {
        $sitemap = new Sitemap;
        $url = new Sitemap_URL;

        Minion_CLI::write(self::SITEMAP_TITLE_CONTENT_GENERATE. $name);
        foreach($model as $item) {
            $content = '';
            $newUrl = self::SITEMAP_URL_CONTENT . "/company/view/" . $item->id;
            $url->set_loc($newUrl)
                ->set_priority(self::SITEMAP_PRIORITY_9)
                ->set_last_mod(time())
                ->set_change_frequency(self::SITEMAP_LAST_MOD_ALWAYS);
            $sitemap->add($url);
        }
       
        return $response = urldecode($sitemap->render());
    }

    /**
     * Получаем модель данный по названию.
     * @param $name
     * @return Database_Result
     */
    private static function getModelForName($name) {
        return ORM::factory($name)->find_all();
    }

    /**
     * Запись в конкретный файл
     * @param $file
     * @param $response
     * @return bool
     */
    public function FilePutContents($file, $response) {
       
        if($response == null) {
          return false;
        }
        
        file_put_contents( DOCROOT . "sitemap/$file.xml", $response);
        return true;
    }
}