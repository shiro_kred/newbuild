<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Npc extends Template_ViewPublicAdmin {

    public $template = 'app';

    public function action_index() {
        $Count         = ORM::factory('Book_Data_Personal')->count_all();
        $pagination    = Pagination::factory(array(
            'total_items'    => $Count,
            'items_per_page' => 10
        ));
        $Model         = ORM::factory('Book_Data_Personal') ->limit($pagination->items_per_page)->offset($pagination->offset)->find_all();
        $this->content = View::factory($this->view_path)->bind('Model', $Model);
    }

    public function action_add() {
        $Data = Arr::extract($this->request->post(), Kohana_Book::getAllType(), null);
        $Role = Arr::get($this->request->post(), 'role', null);
        $Name = Arr::get($this->request->post(), 'name', null);

        $Values = [
            'name'        => $Name,
            'description' => json_encode($Data),
            'role'        => $Role
        ];

        $Model = ORM::factory('Book_Persona');
        $Model->values($Values);
        $Model->save();
        $this->redirect('/admin/npc');
    }

}