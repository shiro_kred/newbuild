<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Personal extends Template_ViewPublicAdmin {

    public $template = 'app';

    public function action_index() {
        $Count         = ORM::factory('Book_Data_Personal')->count_all();
        $pagination    = Pagination::factory(array(
            'total_items'    => $Count,
            'items_per_page' => 10
        ));
        $Model         = ORM::factory('Book_Data_Personal') ->limit($pagination->items_per_page)->offset($pagination->offset)->find_all();
        $this->content = View::factory($this->view_path)->bind('Model', $Model);
    }
    
    public function action_add() {
        $Data = Arr::extract($this->request->post(), ['type', 'description']);

        if($Data['type'] && $Data['description']) {
            $Model = ORM::factory('Book_Data_Personal');
            $Model->values($Data);
            $Model->save();
            $this->redirect('/admin/personal');
        }
    }
 
}