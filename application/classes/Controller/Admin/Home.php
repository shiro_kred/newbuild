<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Home extends Template_ViewPublicAdmin {

    public $template = 'app';

    public function action_index() {
        $this->content = View::factory($this->view_path);
    }

}