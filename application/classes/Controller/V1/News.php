<?php defined('SYSPATH') or die('No direct script access.');

class Controller_V1_News extends Template_JsonPublic {

    public function before()
    {
        parent::before();

        $this->version = Kohana_Api_Field::API_1;
        $this->message = Kohana_Api_Field::API_1_MSG;
        $this->method  = [
            'name'   => (Request::initial()->controller()),
            'action' => (Request::initial()->action())
        ];
    }

    public function action_index()
    {
        $this->status = true;
    }

    public function action_getPost() {
        $offset = Arr::get($_GET, 'offset', 0);
        $limit  = Arr::get($_GET, 'limit', 5);

        $ModelNews = ORM::factory('News_Post')
            ->limit($limit)
            ->offset($offset)
            ->find_all();

        $Response = [];
        foreach ($ModelNews as $key => $item) {

            $description = strip_tags($item->description);
            $description = preg_replace('/ {2,}/',' ', $description);
            $description = str_replace([' Фото: hawk.ru', ' Источник: ИА «Город55»&nbsp;', ' Фото: иллюстрация'], '', $description);

            $Response[] = [
                'id'            => $item->id,
                'date'          => $item->date,
                'title'         => $item->title,
                'image'         => $item->image,
                'description'   => $description,
            ];
        }

        $this->data = $Response;
        $this->status = true;
    }
}
