<?php defined('SYSPATH') or die('No direct script access.');

class Controller_V1_Status extends Template_JsonPublic {

    public function before()
    {
        parent::before();

        $this->version = Kohana_Api_Field::API_1;
        $this->message = Kohana_Api_Field::API_1_MSG;
        $this->method = [
            'name'   => (Request::initial()->controller()),
            'action' => (Request::initial()->action())
        ];
    }

    public function action_index()
    {
        $this->status = true;
    }
}
