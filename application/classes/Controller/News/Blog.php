<?php defined('SYSPATH') or die('No direct script access.');

class Controller_News_Blog extends Template_ViewPublic {

    public $template = 'app';


    public function action_item() {
        $id = $this->request->param('id');

        if(!$id) {
            throw new HTTP_Exception_404;
        }

        $ModelNews = ORM::factory('News_Post')
            ->where('id', '=', $id)
            ->and_where('region', 'LIKE', $this->region)
            ->find();
        
        if(!$ModelNews->loaded()) {
            throw new HTTP_Exception_404;
        }

        $url = 'http://' . $_SERVER['SERVER_NAME'] . '/blog/item/'. $ModelNews->id;

        $this->template->title        = $ModelNews->title;
        $this->template->keywords     = str_replace(' ', ',', $ModelNews->title);
        $this->template->description  = Filter::str_row($ModelNews->description);
        $this->template->url          = $url;
        $this->template->image        = Filter::str_row($ModelNews->get_image());
        $this->content      = View::factory($this->view_path)->bind('BlogItem', $ModelNews);
    }

}