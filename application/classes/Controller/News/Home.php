<?php defined('SYSPATH') or die('No direct script access.');

class Controller_News_Home extends Template_ViewPublic {

    public $template = 'app';

    public function action_index() {

        $CountNews = ORM::factory('News_Post')
            ->where('region', 'LIKE', $this->region)
            ->order_by('date', 'DESC')
            ->limit(6)
            ->count_all();

        $pagination = Pagination::factory(array(
            'total_items'    => $CountNews,
            'items_per_page' => 15
        ));

        $ModelNews = ORM::factory('News_Post')
            ->where('region', 'LIKE', $this->region)
            ->order_by('date', 'DESC')
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->find_all();

        $url = 'http://' . $_SERVER['SERVER_NAME'];

        $this->template->title        = 'Новости';
        $this->template->keywords     = 'Новости,здесь,сейчас';
        $this->template->description  = 'Только актуальные новости';
        $this->template->url          = $url;
//        $this->template->image        = Filter::str_row($ModelNews->get_image());

        $this->content = View::factory($this->view_path)
            ->bind('pagination', $pagination)
            ->bind('ModelNews', $ModelNews);
    }

}