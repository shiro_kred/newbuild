<?php defined('SYSPATH') or die('No direct script access.');

class Template_JsonPrivate extends Template_JsonPublic {
    
    public $user;

    public function before()
    {
        parent::before();

        if(!Auth::instance()->logged_in())
            throw new HTTP_Exception_404();
        
        $this->user = Auth::instance()->get_user();
    }
}