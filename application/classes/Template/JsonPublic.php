<?php defined('SYSPATH') or die('No direct script access.');

class Template_JsonPublic extends Controller {

    public $status = false, $message, $method, $data, $query, $user, $version = '0.0.1', $error = 0;

    public function before()
    {
        parent::before();

        if(Auth::instance()->logged_in())
            $this->user = Auth::instance()->get_user();

        if (isset($_POST['header']) && ($_POST['header']['appKey'] == '001') && ($_POST['header']['appUser'] == 'appLftUser') && ($_POST['header']['appPass'] == 'appLftPass')) {

        } else {
            if (!isset($_SERVER['PHP_AUTH_USER'])) {
                header("WWW-Authenticate: Basic realm=\"Private Area\"");
                header("HTTP/1.0 401 Unauthorized");
                die("Sorry - you need valid credentials to be granted access!\n");
            } else {
                if (($_SERVER['PHP_AUTH_USER'] == 'apilft') && ($_SERVER['PHP_AUTH_PW'] == 'apilft')) {

                } else {
                    header("WWW-Authenticate: Basic realm=\"Private Area\"");
                    header("HTTP/1.0 401 Unauthorized");
                    die("Sorry - you need valid credentials to be granted access!\n");
                }
            }
        }
    }

    public function after()
    {
        $result = [
            'status'     => $this->status,
            'error'      => $this->error,
            'message'    => $this->message,
            'version'    => $this->version,
            'method'     => $this->method,
            'query'      => $this->query,
            'data'       => $this->data,
//            'client'     => $_SERVER, // Debug
        ];
        header('Access-Control-Allow-Origin: *', false);
        header('Content-Type: application/json; charset=UTF-8', false);
//        $this->response->headers('Content-Type', 'application/json');
        $this->response->body(json_encode($result));

        parent::after();
    }
}