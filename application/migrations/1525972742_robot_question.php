<?php defined('SYSPATH') OR die('No direct script access.');
/**
* create_table($table_name, $fields, array('id' => TRUE, 'options' => ''))
* drop_table($table_name)
* rename_table($old_name, $new_name)
* add_column($table_name, $column_name, $params)
* rename_column($table_name, $column_name, $new_column_name)
* change_column($table_name, $column_name, $params)
* remove_column($table_name, $column_name)
* add_index($table_name, $index_name, $columns, $index_type = 'normal')
* remove_index($table_name, $index_name)
*/
class Robot_Question extends Migration
{
    public function up()
    {
        $this->create_table('robot_question', [
            'id'          => 'primary_key',
            'name'        => ['type' => 'varchar(255)', 'null' => FALSE],
            'date'        => ['type' => 'timestamp', 'null' => FALSE, 'default' => 'CURRENT_TIMESTAMP'],
            'view'        => ['type' => 'int(11)', 'null' => TRUE, 'default' => 'NULL'],
            'data'   => ['type' => 'text', 'null' => TRUE, 'default' => 'NULL'],
            'tags'   => ['type' => 'varchar(255)', 'null' => TRUE, 'default' => 'NULL'],
            'status' => ['type' => 'varchar(255)', 'null' => TRUE, 'default' => 'NULL'],
        ]);
    }

    public function down()
    {
        $this->drop_table('robot_question');
    }
}
