<?php defined('SYSPATH') OR die('No direct script access.');
/**
/Applications/MAMP/bin/php/php5.6.10/bin/php www/index.php --task=db:migrate
/Applications/MAMP/bin/php/php5.6.10/bin/php www/index.php --task=db:generate --name=create_table_user_profile
* create_table($table_name, $fields, array('id' => TRUE, 'options' => ''))
* drop_table($table_name)
* rename_table($old_name, $new_name)
* add_column($table_name, $column_name, $params)
* rename_column($table_name, $column_name, $new_column_name)
* change_column($table_name, $column_name, $params)
* remove_column($table_name, $column_name)
* add_index($table_name, $index_name, $columns, $index_type = 'normal')
* remove_index($table_name, $index_name)
*/
class Tables extends Migration
{
    public function up()
    {
        $this->create_table('news_post', [
            'id'          => 'primary_key',
            'title'       => ['type' => 'varchar(255)', 'null' => FALSE],
            'date'        => ['type' => 'timestamp', 'null' => FALSE, 'default' => 'CURRENT_TIMESTAMP'],
            'image'       => ['type' => 'varchar(1024)', 'null' => TRUE, 'default' => 'NULL'],
            'description' => ['type' => 'text', 'null' => TRUE, 'default' => 'NULL'],

            'tags'   => ['type' => 'varchar(255)', 'null' => TRUE, 'default' => 'NULL'],
            'data'   => ['type' => 'varchar(255)', 'null' => TRUE, 'default' => 'NULL'],
            'status' => ['type' => 'varchar(255)', 'null' => TRUE, 'default' => 'NULL'],
            
            'parse_id'   => ['type' => 'varchar(255)', 'null' => TRUE, 'default' => 'NULL'],
            'parse_link' => ['type' => 'varchar(1024)', 'null' => TRUE, 'default' => 'NULL'],
        ]);
    }

    public function down()
    {
        $this->drop_table('news_post');
    }
}
