<?= View::factory('news/menu')?>
<?= View::factory('news/header')?>

<div class="main main-raised">
    <div class="container">

        <div class="section col-sm-12">
            <h1>Персонаж</h1>
            <?= Form::open('/admin/npc/add', ['method' => 'POST'])?>
            <br>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Волосы</label>
                    <?= Form::input(Kohana_Book::FIELD_HAIR, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Лицо</label>
                    <?= Form::input(Kohana_Book::FIELD_FACE, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Глаза</label>
                    <?= Form::input(Kohana_Book::FIELD_EYES, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Нос</label>
                    <?= Form::input(Kohana_Book::FIELD_NOSE, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Рот</label>
                    <?= Form::input(Kohana_Book::FIELD_MOUTH, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <hr>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Плечи</label>
                    <?= Form::input(Kohana_Book::FILED_SHOULDER, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Торс</label>
                    <?= Form::input(Kohana_Book::FILED_TORSO, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Пояс</label>
                    <?= Form::input(Kohana_Book::FILED_WAIST, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Ноги</label>
                    <?= Form::input(Kohana_Book::FILED_LEGS, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Руки</label>
                    <?= Form::input(Kohana_Book::FILED_HAND, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <hr>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Шапка</label>
                    <?= Form::input(Kohana_Book::FIELD_HEIR_EQUIPMENT, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Наплечники</label>
                    <?= Form::input(Kohana_Book::FILED_SHOULDER_EQUIPMENT, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Тело</label>
                    <?= Form::input(Kohana_Book::FILED_TORSO_EQUIPMENT, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Пояс</label>
                    <?= Form::input(Kohana_Book::FILED_WAIST_EQUIPMENT, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Ноги</label>
                    <?= Form::input(Kohana_Book::FILED_LEGS_EQUIPMENT, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Руки</label>
                    <?= Form::input(Kohana_Book::FILED_HAND_EQUIPMENT, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Другое обмундирование</label>
                    <?= Form::input(Kohana_Book::FILED_OTHER_EQUIPMENT, '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label">Оружие</label>
                    <?= Form::input(Kohana_Book::FILED_WEAPON_EQUIPMENT, '', ['class'=> 'form-control'])?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group label-floating">
                    <label class="control-label">Имя</label>
                    <?= Form::input('name', '', ['class'=> 'form-control'])?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group label-floating">
                    <label class="control-label">Роль\Расса</label>
                    <?= Form::input('role', '', ['class'=> 'form-control'])?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Создать</button>
                </div>
            </div>
            <?= Form::close()?>
        </div>

    </div>

</div>


<?= View::factory('news/footer')?>
