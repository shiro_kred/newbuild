<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php $version = '?' . Kohana::$config->load('application')->get('version');?>
<?php $statics_url_theme = "/media/theme/";?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title><?= isset($title) ? $title : ''?></title>
    <meta name="keywords" content="<?= isset($keywords) ? $keywords : ''?>">
    <meta name="description" content="<?= isset($description) ? $description : ''?>">
    <meta property="og:title" content="<?= isset($title) ? $title : ''?>">
    <meta property="og:url" content="<?= isset($url) ? $url : ''?>">
    <meta property="og:image" content="<?= isset($image) ? $image : ''?>">
    <meta property="og:description" content="<?= isset($description) ? $description : ''?>">

    <meta name="yandex-verification" content="0f3ba5ee80f7277e" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link href="<?=$statics_url_theme?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=$statics_url_theme?>assets/css/material-kit.css" rel="stylesheet"/>

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?=$statics_url_theme?>assets/assets-for-demo/vertical-nav.css" rel="stylesheet" />
    <link href="/media/news/build/app.min.css" rel="stylesheet" />

</head>

<body class="<?php
switch (strtolower(Request::initial()->action())) {
    case "signin":
        echo "login-page";
        break;
    case "signup":
        echo "signup-page";
        break;
    default:
        echo "section-white";
        break;
}
?>">

    <?= isset($content) ? $content : "" ?>
</body>
<!--   Core JS Files   -->
<script src="<?=$statics_url_theme?>assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?=$statics_url_theme?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=$statics_url_theme?>assets/js/material.min.js"></script>

<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?=$statics_url_theme?>assets/js/nouislider.min.js" type="text/javascript"></script>

<!--	Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
<script src="<?=$statics_url_theme?>assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

<!--	Plugin for Select Form control, full documentation here: https://github.com/FezVrasta/dropdown.js -->
<script src="<?=$statics_url_theme?>assets/js/jquery.dropdown.js" type="text/javascript"></script>

<!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/  -->
<script src="<?=$statics_url_theme?>assets/js/jquery.tagsinput.js"></script>

<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?=$statics_url_theme?>assets/js/jasny-bootstrap.min.js"></script>

<!-- Plugin For Google Maps -->
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>-->

<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="<?=$statics_url_theme?>assets/js/material-kit.js" type="text/javascript"></script>

<!-- Fixed Sidebar Nav - JS For Demo Purpose, Don't Include it in your project -->
<script src="<?=$statics_url_theme?>assets/assets-for-demo/modernizr.js" type="text/javascript"></script>
<script src="<?=$statics_url_theme?>assets/assets-for-demo/vertical-nav.js" type="text/javascript"></script>

<script type="text/javascript">
    $().ready(function(){

//        materialKitDemo.initContactUs2Map();
    });
</script>

<!--<script src="/media/base/js/libs/underscore/underscore-min.js" type="text/javascript"></script>-->
<!--<script src="/media/base/js/libs/backbone/backbone.min.js" type="text/javascript"></script>-->
<!--<script src="/media/base/app/app.js--><?//=$version?><!--" type="text/javascript"></script>-->
<?php
if(isset($additional_script)){
    foreach ($additional_script as $item) {
        echo HTML::script($item.$version);
    }
}
?>

<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter44506039 = new Ya.Metrika({ id:44506039, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/44506039" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-49460247-5', 'auto');
    ga('send', 'pageview');

</script>
</html>
