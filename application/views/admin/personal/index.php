<?= View::factory('news/menu')?>
<?= View::factory('news/header')?>

<div class="main main-raised">
    <div class="container">

        <div class="section col-sm-12">
            <?= Form::open('/admin/personal/add', ['method' => 'POST'])?>
            <div class="col-sm-3">
                <div class="select form-group">
                    <select name="type" class="select form-control">
                        <option disabled selected> Выбор типа</option>
                        <?php foreach (Kohana_Book::getPersonalType() as $item => $key):?>
                            <option value="<?=$item?>"><?=$key?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="form-group">
                    <?= Form::input('description', '', ['class'=> 'form-control'])?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-xs">Создать</button>
                </div>
            </div>
            <?= Form::close()?>
        </div>

        <table class="table">
            <thead>
            <tr>
                <th class="text-center">#</th>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-right">Actions</th>
            </tr>
            </thead>
            <tbody> 
            <?php foreach ($Model as $item):?>
                <tr>
                    <td class="text-center"><?=$item->id ?></td>
                    <td><?= Kohana_Book::getPersonalType()[$item->type] ?></td>
                    <td><?= $item->index ?></td>
                    <td><?= Filter::str_row($item->description, 150) ?></td>
                    <td class="td-actions text-right">
                        <button type="button" rel="tooltip" class="btn btn-info">
                            <i class="material-icons">person</i>
                        </button>
                        <button type="button" rel="tooltip" class="btn btn-success">
                            <i class="material-icons">edit</i>
                        </button>
                        <button type="button" rel="tooltip" class="btn btn-danger">
                            <i class="material-icons">close</i>
                        </button>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>

</div>


<?= View::factory('news/footer')?>
