<footer class="footer">
    <div class="container">
        <a class="footer-brand" href="/"><?= Kohana::$config->load('application')->get('name');?></a>


        <ul class="pull-center">
            <li>
                <a href="/">
                    Главная
                </a>
            </li>
            <li>
                <a href="http://tyumen.<?= Kohana::$config->load('application')->get('server');?>">Тюмень</a>
            </li>
            <li>
                <a href="http://tomsk.<?= Kohana::$config->load('application')->get('server');?>">Томск</a>
            </li>
            <li>
                <a href="http://omsk.<?= Kohana::$config->load('application')->get('server');?>">Омск</a>
            </li>
            <li>
                <a href="http://nsk.<?= Kohana::$config->load('application')->get('server');?>">Новосибирск</a>
            </li>
        </ul>

        <ul class="social-buttons pull-right">
            <li>
                <a href="#" target="_blank" class="btn btn-just-icon btn-twitter btn-simple">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <li>
                <a href="#" target="_blank" class="btn btn-just-icon btn-facebook btn-simple">
                    <i class="fa fa-facebook-square"></i>
                </a>
            </li>
            <li>
                <a href="#" target="_blank" class="btn btn-just-icon btn-google btn-simple">
                    <i class="fa fa-google"></i>
                </a>
            </li>
        </ul>

    </div>
</footer>

