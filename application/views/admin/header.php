<?php $bg_url = '/media/theme/assets/img/bg' . rand(0,12) . '.jpg'; ?>

<div class="page-header header-filter header-small" data-parallax="active" style="background-image: url('<?= $bg_url ?>');">
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 text-center">
                <h1 class="title">A Place for Entrepreneurs to Share and Discover New Stories</h1>
            </div>
        </div>
    </div>
</div>