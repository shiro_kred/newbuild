<?= View::factory('news/menu')?>
<?= View::factory('news/header')?>

<div class="main main-raised">
    <div class="container">

        <div class="section col-sm-12">
            <h3 class="title text-center">Новости и ничего лишнего</h3>
            <br />
            <div class="col-sm-10 col-sm-offset-1">
                <div class="row home-list">
                    <?php foreach ($ModelNews as $item):?>
                        <div class="col-md-4 col-xs-12">
                            <div class="card card-plain card-blog">
                                <div class="card-image">
                                    <a href="/blog/item/<?=$item->id?>">
                                        <div class="img img-raised" style="background-image: url('<?= $item->get_image();?>')"></div>
                                    </a>
                                </div>

                                <div class="content">
                                    <!--                                <h6 class="category text-info">Enterprise</h6>-->
                                    <h4 class="card-title">
                                        <a href="/blog/item/<?=$item->id?>"><?= $item->title;?></a>
                                    </h4>
                                    <p class="card-description">
                                        <?= Filter::str_row($item->description);?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="pagination-block">
                <?= ($pagination) ? $pagination : "" ?>
            </div>
        </div>
    </div>

</div>

<?= View::factory('news/footer')?>

