<?= View::factory('news/menu')?>
<?= View::factory('news/header')?>

<div class="main main-raised">
    <div class="container">
        <div class="section section-text">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <a href="/" class="btn btn-info">
                        На главную
                    </a>
                </div>
                <div class="col-xs-8 col-xs-offset-2">
                    <h3 class="title"><?= $BlogItem->title?></h3>
                </div>

                <div class="col-xs-8 col-xs-offset-2">
                    <?php
                        $description = $BlogItem->description;
                        $description = str_replace('<a', '<noindex><a', $description);
                        $description = str_replace('</a>', '</a><noindex>', $description);
                        $description = preg_replace('/(style=[^\s>]+)/is', '', $description);
                    ?>
                   <?= $description?>
                </div>

                <?php if(isset($BlogItem->image) && ($BlogItem->image != '')):?>
                    <div class="section col-xs-8 col-xs-offset-2">
                        <img class="img-rounded img-responsive img-raised" alt="Raised Image" style="width: 100%" src="<?= $BlogItem->image?>">
                    </div>
                <?php endif;?>
                <div class="col-xs-8 col-xs-offset-2">
                    Дата публикации: <?= date("Y-m-d", strtotime($BlogItem->date));?>
                </div>
                <div class="col-xs-8 col-xs-offset-2">
                    <a href="/" class="btn btn-primary">
                        К главной странице
                    </a>
                </div>

<!--                <div class="col-md-8 col-md-offset-2">-->
<!---->
<!--                    <div class="row">-->
<!--                        <div class="col-md-6">-->
<!--                            <div class="blog-tags">-->
<!--                                Tags:-->
<!--                                <span class="label label-primary">Photography</span>-->
<!--                                <span class="label label-primary">Stories</span>-->
<!--                                <span class="label label-primary">Castle</span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-6">-->
<!--                            <a href="#pablo" class="btn btn-google btn-round pull-right">-->
<!--                                <i class="fa fa-google"></i> 232-->
<!--                            </a>-->
<!--                            <a href="#pablo" class="btn btn-twitter btn-round pull-right">-->
<!--                                <i class="fa fa-twitter"></i> 910-->
<!--                            </a>-->
<!--                            <a href="#pablo" class="btn btn-facebook btn-round pull-right">-->
<!--                                <i class="fa fa-facebook-square"></i> 872-->
<!--                            </a>-->
<!---->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">


            <div class="col-xs-12">
                <h2 class="title text-center">Последние новости</h2>
                <br />
                <div class="row">
                    <?php foreach (ORM::factory('News_Post')
                                        ->where('id', '!=', $BlogItem->id)
                                        ->order_by('date', 'DESC')
                                        ->limit(3)
                                        ->find_all() as $item):?>
                        <div class="col-md-4">
                            <div class="card card-plain card-blog">
                                <div class="card-image">
                                    <a href="/blog/item/<?=$item->id?>">
                                        <div class="img img-raised" style="background-image: url('<?= $item->get_image();?>')"></div>
                                    </a>
                                </div>

                                <div class="content">
<!--                                    <h6 class="category text-info">Enterprise</h6>-->
                                    <h4 class="card-title">
                                        <a href="/blog/item/<?=$item->id?>"><?= $item->title;?></a>
                                    </h4>
                                    <p class="card-description">
                                        <?= Filter::str_row($item->description);?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= View::factory('news/footer')?>
